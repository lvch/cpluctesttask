<?php

$GLOBALS = parse_ini_file("app.ini");
require __DIR__ . "/vendor/autoload.php";

use App\CurrencyController;

CurrencyController::create()->run($argv[1]);
