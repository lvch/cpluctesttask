<?php


use App\CurrencyController;
use PHPUnit\Framework\TestCase;

class CurrencyControllerTest extends TestCase
{

    public function testRoundUp() {
        $this->assertEquals(
            0.44,
            CurrencyController::create()
                ->roundUp(0.432354535, 2)
        );

        $this->assertEquals(
            0.44,
            CurrencyController::create()
                ->roundUp(0.43000001, 2)
        );

        $this->assertEquals(
            1,
            CurrencyController::create()
                ->roundUp(0.999999999, 2)
        );
    }

    /**
     * @dataProvider additionProvider
     * @depends testRoundUp
     */
    public function testRun($row)
    {
        $GLOBALS = parse_ini_file("../app.ini");
        $totalRow = '';

        foreach (explode("\n", $row) as $item) {
            if (!empty($item)) {
                $item = json_decode($item, true);
                $rate = @json_decode(
                    file_get_contents('https://api.exchangeratesapi.io/latest'),
                    true)['rates'][$item['currency']];

                if ($item['currency'] == 'EUR' or $rate == 0) {
                    $amntFixed = $item['amount'];
                } else if ($item['currency'] != 'EUR' or $rate > 0) {
                    $amntFixed = $item['amount'] / $rate;
                }

                $totalRow = $totalRow . CurrencyController::create()
                        ->roundUp($amntFixed * ($item['isEU'] === 'true' ? 0.01 : 0.02), 2) . "\n";
            }
        }

        $this->expectOutputString($totalRow);
        CurrencyController::create()->run('../input.txt');
    }

    public function additionProvider()
    {
        return [
            ['{"bin":"45717360","amount":"100.00","currency":"EUR","isEU":"true"}
{"bin":"516793","amount":"50.00","currency":"USD","isEU":"true"}
{"bin":"45417360","amount":"10000.00","currency":"JPY","isEU":"false"}
{"bin":"41417360","amount":"130.00","currency":"USD","isEU":"false"}
{"bin":"4745030","amount":"2000.00","currency":"GBP","isEU":"false"}
']
        ];
    }
}
