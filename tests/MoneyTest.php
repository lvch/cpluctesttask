<?php


use App\Money;

class MoneyTest extends PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider additionalData
     */
    public function testIsEu($country, $inEU)
    {
        $this->assertEquals($inEU, Money::isEu($country));
    }

    public function additionalData() {
        return [
            ['RU', false],
            ['DE', true],
            ['EE', true],
            ['ES', true],
            ['FI', true],
            ['FR', true],
            ['GR', true],
            ['JP', false],
            ['', false]
        ];
    }
}
