<?php


namespace App;

class CurrencyController
{
    use SelfStaticFactory;
    use CeilValues;

    private $trans;
    private $binReq;
    private $rateReq;

    const SMALL_COMMISSION = 0.01;
    const BIG_COMMISSION = 0.02;
    const EUR = 'EUR';

    /**
     * @param string $inputFile file name
     * Method for calculating and printing commission in EU
     */
    public function run($inputFile) {
        array_map(function ($value) {
            $this->checkTransaction($value);
        },explode(
            "\n",
            file_get_contents($inputFile)
        ));
    }

    /**
     * @param string $row row from input data
     * Method for partially calculating and printing commission in EU
     */
    private function checkTransaction($row) {
        if (!empty($row)) {
            $this->trans = new Transaction($row);

            $this->setupBinReq($this->trans);
            $this->setupRateReq($this->trans);

            $this->printCommission();
        }
    }

    /**
     * @param TransactionInterface $transaction
     * Method for sending request to external URL (BIN provider)
     */
    private function setupBinReq(TransactionInterface $transaction) {
        $this->binReq = new BinResultRequest($this->trans);
        $this->binReq->doRequest($GLOBALS['bin_results'] . $this->trans->getBin());
    }

    /**
     * @param TransactionInterface $transaction
     * Method for sending request to external URL (RATE provider)
     */
    private function setupRateReq(TransactionInterface $transaction) {
        $this->rateReq = new RateRequest($this->trans);
        $this->rateReq->doRequest($GLOBALS['rate']);
    }

    /**
     * Method for printing total commission according to country code and rates
     */
    private function printCommission() {
        if ($this->trans->getCurrency() === self::EUR or $this->rateReq->getData() === 0) {
            $amntFixed = $this->trans->getAmount();
        }
        else if ($this->trans->getCurrency() !== self::EUR or $this->rateReq->getData() > 0) {
            $amntFixed = $this->trans->getAmount() / $this->rateReq->getData();
        }

        echo $this->roundUp($amntFixed * (
            Money::isEu(
                json_decode($this->binReq->getData())
                    ->country
                    ->alpha2
            )
                ? self::SMALL_COMMISSION
                : self::BIG_COMMISSION
            ), 2);
        print "\n";
    }
}