<?php


namespace App;


class Transaction implements TransactionInterface
{
    private $bin;
    private $amount;
    private $currency;

    const BIN = 'bin';
    const AMOUNT = 'amount';
    const CURRENCY = 'currency';

    public function __construct($transaction) {
        $this->setVariables(
            @json_decode($transaction, true)
        );
    }

    /**
     * @param mixed $vars array of transaction
     * Setting variables
     */
    private function setVariables($vars) {
        $this->setBin(
            $vars ? $vars[self::BIN] : null
        );

        $this->setAmount(
            $vars ? $vars[self::AMOUNT] : null
        );

        $this->setCurrency(
            $vars ? $vars[self::CURRENCY] : null
        );
    }

    public function setBin($value) {
        $this->bin = $value;
    }

    public function getBin() {
        return $this->bin;
    }

    public function setAmount($value) {
        $this->amount = $value;
    }

    public function getAmount() {
        return $this->amount;
    }

    public function setCurrency($value) {
        $this->currency = $value;
    }

    public function getCurrency() {
        return $this->currency;
    }
}