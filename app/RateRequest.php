<?php


namespace App;


class RateRequest extends Request
{
    /**
     * @return mixed|string|null
     * Getting rate for current transaction
     */
    public function getData() {
        return @json_decode(parent::getData(),true)['rates'][$this->trans->getCurrency()];
    }
}