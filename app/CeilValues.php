<?php


namespace App;


trait CeilValues
{
    /**
     * @param int|float $value
     * @param int $places
     * @return float|int
     *
     * Round number to a higher value
     */
    public function roundUp ($value, $places = 0) {
        if ($places < 0) {
            $places = 0;
        }

        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }
}