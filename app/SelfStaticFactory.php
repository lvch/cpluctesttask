<?php


namespace App;


trait SelfStaticFactory
{
    public static function create() {
        return new static(...func_get_args());
    }
}