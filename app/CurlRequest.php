<?php


namespace App;


trait CurlRequest
{
    private $curl;
    private $data;

    /**
     * @param string $address external URL
     * Make request and get data from external URL
     */
    public function doRequest($address) {
        $this->setCurl($address);
        $this->setOpt();
        $this->setData(curl_exec($this->getCurl()));
        $this->closeCurl();
    }

    /**
     * setup Curl settings
     */
    private function setOpt() {
        curl_setopt($this->getCurl(),CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->getCurl(), CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->getCurl(), CURLOPT_HEADER, false);
    }

    /**
     * close Curl
     */
    private function closeCurl() {
        curl_close($this->getCurl());
    }

    /**
     * @param string $addr external URL
     * create Curl
     */
    public function setCurl($addr) {
        $this->curl = curl_init($addr);
    }

    /**
     * @return resource|false
     */
    public function getCurl() {
        return $this->curl;
    }

    /**
     * @param string|null $value
     */
    public function setData($value) {
        $this->data = $value;
    }

    /**
     * @return string|null
     */
    public function getData() {
        return $this->data;
    }
}