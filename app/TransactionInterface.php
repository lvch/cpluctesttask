<?php


namespace App;


interface TransactionInterface
{
    public function setBin($value);

    public function getBin();

    public function setAmount($value);

    public function getAmount();

    public function setCurrency($value);

    public function getCurrency();
}