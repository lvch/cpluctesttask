<?php

namespace App;

class Money
{
    // European countries
    const EU_COUNTRIES = [
        'AT',
        'BE',
        'BG',
        'CY',
        'CZ',
        'DE',
        'DK',
        'EE',
        'ES',
        'FI',
        'FR',
        'GR',
        'HR',
        'HU',
        'IE',
        'IT',
        'LT',
        'LU',
        'LV',
        'MT',
        'NL',
        'PO',
        'PT',
        'RO',
        'SE',
        'SI',
        'SK'
    ];

    /**
     * @param string $c country code
     * @return bool
     * Check if country in Europe
     */
    public static function isEu($c) {
        return in_array($c, self::EU_COUNTRIES);
    }
}