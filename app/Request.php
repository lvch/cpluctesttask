<?php


namespace App;


abstract class Request
{
    use CurlRequest;
    protected $trans;

    public function __construct(TransactionInterface $transaction) {
        $this->trans = $transaction;
    }
}